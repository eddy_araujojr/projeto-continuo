package com.itau.fizzbuzz;

/**
 * Hello world!
 *
 */

public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        int i = 0;
        if(args.length > 0)
            i = Integer.parseInt(args[0]);
        if(i > 0)
            FizzBuzz.fatorialFizzBuzz(i);
    }
}
