package com.itau.fizzbuzz;

public class FizzBuzz {

	public static String fizzBuzz(int i) {
		if ((i % 3 == 0) && (i % 5 == 0))
			return "Fizzbuzz";
		if (i % 5 == 0)
			return "Buzz";
		if (i % 3 == 0)
			return "Fizz";		
		return Integer.toString(i);
	}

	public static String fatorialFizzBuzz(int i) {
		String retorno = "";
		for (int j = 1; j <= i; j++)
			retorno = fizzBuzz(j) + " ";
		System.out.println(retorno);
		return retorno;
	}
}
